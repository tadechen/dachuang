import Vue from 'vue'
import App from './App'


// 实验室电脑ip
// Vue.prototype.websiteUrl = "http://192.168.123.59:8081";

// 笔记本电脑ip
// Vue.prototype.websiteUrl = "http://192.168.43.79:8081";

// 服务器ip
Vue.prototype.websiteUrl = "http://47.100.232.30:8081";

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
